# Extension Template for GNOME Builder
A proof-of-concept for out-of-tree plugins for GNOME Builder.

Uses flatpak for building and distribution.

## Building
You will need to use a custom build of GNOME Builder (with development files and
shared library support) in order to compile and run `builder-extension`. You can
get a flatpak of this build [here](https://gitlab.gnome.org/firox263/gnome-builder/-/jobs/1686641)
temporarily - click 'Download' under 'Job artifacts'.

Dependencies:
 1. `gnome-nightly` flatpak repo installed
 2. Custom GNOME Builder flatpak from above (install with `--user`)
 3. `flatpak-builder`
 
Run the following to build and install the plugin:

```
flatpak-builder _build --install --user --force-clean org.gnome.Builder.Plugin.Example.json
```

You can uninstall the plugin at any time and reset to a "clean slate":

```
flatpak remove org.gnome.Builder.Plugin.Example
```
